
Main Character.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
body
  rotate: false
  xy: 406, 342
  size: 135, 166
  orig: 135, 166
  offset: 0, 0
  index: -1
body2
  rotate: false
  xy: 406, 178
  size: 132, 162
  orig: 133, 163
  offset: 0, 1
  index: -1
chan sau 1
  rotate: true
  xy: 285, 10
  size: 68, 109
  orig: 68, 109
  offset: 0, 0
  index: -1
chan sau 2
  rotate: true
  xy: 406, 91
  size: 85, 50
  orig: 85, 50
  offset: 0, 0
  index: -1
chan truoc 1
  rotate: true
  xy: 2, 2
  size: 76, 107
  orig: 76, 107
  offset: 0, 0
  index: -1
chan truoc 2
  rotate: true
  xy: 460, 6
  size: 83, 48
  orig: 85, 50
  offset: 1, 1
  index: -1
dau
  rotate: true
  xy: 2, 80
  size: 428, 402
  orig: 428, 402
  offset: 0, 0
  index: -1
longmay1
  rotate: false
  xy: 566, 13
  size: 57, 28
  orig: 59, 32
  offset: 1, 2
  index: -1
longmay2
  rotate: true
  xy: 250, 9
  size: 69, 33
  orig: 72, 35
  offset: 1, 1
  index: -1
mat1
  rotate: true
  xy: 617, 65
  size: 59, 57
  orig: 61, 59
  offset: 1, 1
  index: -1
mat2
  rotate: true
  xy: 150, 5
  size: 73, 61
  orig: 75, 63
  offset: 1, 1
  index: -1
mat_die 1
  rotate: false
  xy: 510, 4
  size: 54, 37
  orig: 56, 40
  offset: 1, 1
  index: -1
mat_die 2
  rotate: true
  xy: 111, 2
  size: 76, 37
  orig: 80, 40
  offset: 2, 1
  index: -1
mom1
  rotate: true
  xy: 213, 7
  size: 71, 35
  orig: 74, 37
  offset: 1, 1
  index: -1
mom2
  rotate: true
  xy: 617, 126
  size: 81, 44
  orig: 84, 46
  offset: 2, 1
  index: -1
mom3
  rotate: true
  xy: 458, 91
  size: 85, 50
  orig: 85, 51
  offset: 0, 0
  index: -1
mui
  rotate: true
  xy: 396, 13
  size: 65, 62
  orig: 70, 64
  offset: 1, 1
  index: -1
tay sau
  rotate: true
  xy: 510, 43
  size: 133, 105
  orig: 133, 105
  offset: 0, 0
  index: -1
tay truoc
  rotate: false
  xy: 540, 209
  size: 99, 131
  orig: 99, 131
  offset: 0, 0
  index: -1
