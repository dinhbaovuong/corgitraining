
E4.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
Body
  rotate: true
  xy: 2, 507
  size: 124, 273
  orig: 126, 276
  offset: 1, 1
  index: -1
L_Hand
  rotate: false
  xy: 277, 464
  size: 190, 167
  orig: 193, 170
  offset: 1, 2
  index: -1
Mouth_01
  rotate: true
  xy: 479, 2
  size: 460, 483
  orig: 460, 483
  offset: 0, 0
  index: -1
Mouth_02
  rotate: true
  xy: 2, 30
  size: 432, 475
  orig: 455, 478
  offset: 21, 1
  index: -1
R_Hand
  rotate: false
  xy: 469, 464
  size: 190, 167
  orig: 193, 170
  offset: 2, 2
  index: -1
