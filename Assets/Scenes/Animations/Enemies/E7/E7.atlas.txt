
E7.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
Body
  rotate: false
  xy: 2, 116
  size: 364, 391
  orig: 364, 391
  offset: 0, 0
  index: -1
L_Eye
  rotate: false
  xy: 477, 410
  size: 89, 97
  orig: 92, 100
  offset: 2, 1
  index: -1
Mouth
  rotate: false
  xy: 256, 3
  size: 206, 111
  orig: 208, 115
  offset: 1, 2
  index: -1
Mouth_2
  rotate: true
  xy: 368, 283
  size: 224, 107
  orig: 227, 111
  offset: 2, 2
  index: -1
R_eye
  rotate: true
  xy: 464, 5
  size: 109, 104
  orig: 113, 107
  offset: 2, 1
  index: -1
Tail_1
  rotate: false
  xy: 2, 2
  size: 143, 112
  orig: 146, 115
  offset: 2, 2
  index: -1
Tail_2
  rotate: true
  xy: 368, 116
  size: 165, 162
  orig: 167, 166
  offset: 1, 2
  index: -1
Vay
  rotate: false
  xy: 147, 2
  size: 107, 112
  orig: 110, 114
  offset: 2, 1
  index: -1
