﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace MoreMountains.CorgiEngine
{
	/// <summary>
	/// This class handles the GUI in the action phases of the Retro Adventure levels
	/// </summary>
	public class MarioGUIManager : GUIManager, MMEventListener<CorgiEngineEvent>, MMEventListener<CorgiEngineStarEvent>
	{
		[Header("Mario")]
		/// the text used to display collected stars number
		[Tooltip("the text used to display collected stars number")]
		public Text StarDisplayText;
		/// the splash to display when the level is complete
		[Tooltip("the splash to display when the level is complete")]
		public GameObject LevelCompleteSplash;
		/// the object to give focus to when the complete splash gets displayed
		[Tooltip("the object to give focus to when the complete splash gets displayed")]
		public GameObject LevelCompleteSplashFocus;
		/// the GUI inventory displays
		[Tooltip("the GUI inventory displays")]
		public GameObject Inventories;
		/// the representation of the collected stars
		[Tooltip("the representation of the collected stars")]
		public Image[] Stars;
		/// the color to display a collected star with
		[Tooltip("the color to display a collected star with")]
		public Color StarOnColor;
		/// the color to display a not collected star with
		[Tooltip("the color to display a not collected star with")]
		public Color StarOffColor;
		/// the splash to display when the player is dead
		[Tooltip("the splash to display when the player is dead")]
		public GameObject GameOverSplash;
		/// the object to give focus to when the gameover splash gets displayed
		[Tooltip("the object to give focus to when the gameover splash gets displayed")]
		public GameObject GameOverSplashFocus;

		private bool[] starsAte = { false, false, false };

		/// <summary>
		/// On Update we update our star text
		/// </summary>
		protected virtual void Update()
		{
			UpdateStars();
		}

		/// <summary>
		/// Every frame we update our star text with the current version
		/// </summary>
		protected virtual void UpdateStars()
		{
			//StarDisplayText.text = RetroAdventureProgressManager.Instance.CurrentStars.ToString();
			int n = 0;
			for (int i = 0; i < 3; i++)
			{
				if (starsAte[i])
					n++;
			}
			StarDisplayText.text = n.ToString();
		}

		/// <summary>
		/// When the level is complete we display our level complete splash and set its values
		/// </summary>
		public virtual void LevelComplete()
		{
			EventSystem.current.sendNavigationEvents = true;
			GameManager.Instance.Pause(PauseMethods.NoPauseMenu);
			LevelCompleteSplash.SetActive(true);

			//foreach (RetroAdventureScene scene in RetroAdventureProgressManager.Instance.Scenes)
			//{
			//if (scene.SceneName == SceneManager.GetActiveScene().name)
			//{
				for (int i = 0; i < starsAte.Length; i++)
				{
					Stars[i].color = (starsAte[i]) ? StarOnColor : StarOffColor;
				}

			//}
			//}

			if (LevelCompleteSplashFocus != null)
			{
				EventSystem.current.SetSelectedGameObject(LevelCompleteSplashFocus, null);
			}
		}

		/// <summary>
        /// When the player get eliminated, display this gameover splash and set its value
        /// </summary>
		IEnumerator GameOver(float seconds)
        {
			yield return new WaitForSeconds(seconds);

			EventSystem.current.sendNavigationEvents = true;
			GameManager.Instance.Pause(PauseMethods.NoPauseMenu);
			GameOverSplash.SetActive(true);

			if (GameOverSplashFocus != null)
			{
				EventSystem.current.SetSelectedGameObject(GameOverSplashFocus, null);
			}
		}

		/// <summary>
		/// When grabbing a level complete event, we call our LevelComplete method
		/// </summary>
		/// <param name="corgiEngineEvent">Corgi engine event.</param>
		public virtual void OnMMEvent(CorgiEngineEvent corgiEngineEvent)
		{
			switch (corgiEngineEvent.EventType)
			{
				case CorgiEngineEventTypes.LevelComplete:
					LevelComplete();
					break;
				case CorgiEngineEventTypes.SpawnCharacterStarts:
					starsAte = new bool[] { false, false, false };
					break;
				case CorgiEngineEventTypes.Respawn:
					starsAte = new bool[] { false, false, false };
					break;
				case CorgiEngineEventTypes.GameOver:
					StartCoroutine(GameOver(2f));
					//StopAllCoroutine();
					break;
			}
			//StopAllCoroutine();
		}

		public virtual void OnMMEvent(CorgiEngineStarEvent corgiStarEvent)
		{
			starsAte[corgiStarEvent.StarID] = true;
		}

		protected override void OnEnable()
		{
			base.OnEnable();
			this.MMEventStartListening<CorgiEngineEvent>();
			this.MMEventStartListening<CorgiEngineStarEvent>();
		}

		protected override void OnDisable()
		{
			base.OnDisable();
			this.MMEventStopListening<CorgiEngineEvent>();
			this.MMEventStopListening<CorgiEngineStarEvent>();
		}
	}
}
