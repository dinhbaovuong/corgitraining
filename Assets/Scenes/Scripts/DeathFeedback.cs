﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;
using DG.Tweening;

namespace MoreMountains.CorgiEngine
{
    public class DeathFeedback : MonoBehaviour
    {
        public Health EnemyHealth;
        public Transform EnemyTransform;
        public AIWalk EnemyWalk;
        public CorgiController EnemyController;
        public float ScaleY = 0.1f;
        public float animDuration = 0.2f;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (EnemyHealth.CurrentHealth <= 0)
            {
                EnemyController.enabled = false;
                EnemyWalk.enabled = false;
                EnemyTransform
                .DOScaleY(ScaleY, animDuration);
                EnemyWalk.enabled = false;
            }

        }
    }
}
