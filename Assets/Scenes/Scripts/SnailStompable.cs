﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
	[RequireComponent(typeof(BoxCollider2D))]
	[RequireComponent(typeof(Health))]

	/// <summary>
	/// Add this class to an snail enemy (or whatever you want), to be able to stomp on it twice.
	/// </summary>
	[AddComponentMenu("Corgi Engine/Character/Damage/SnailStompable")]
	public class SnailStompable : Stompable
	{
		//public Coroutine StartCoroutine(IEnumerator routine);

		protected override void PerformStomp(CorgiController corgiController)
		{
			if (DamageCausedKnockbackType == KnockbackStyles.SetForce)
			{
				corgiController.SetForce(KnockbackForce);
			}
			if (DamageCausedKnockbackType == KnockbackStyles.AddForce)
			{
				corgiController.AddForce(KnockbackForce);
			}

			if (_health != null)
			{
				_health.Damage(DamagePerStomp, corgiController.gameObject, InvincibilityDuration, InvincibilityDuration);

				//Character>().enabled = false;
				GetComponent<CorgiController>().enabled = false;
				GetComponent<DamageOnTouch>().enabled = false;
				GetComponent<CharacterHorizontalMovement>().enabled = false;
				GetComponent<AIWalk>().enabled = false;
				
				StartCoroutine(ActiveAfter(4f));

			}
			// if what's colliding with us has a CharacterJump component, we reset its JumpButtonReleased flag so that the knockback effect is applied correctly.
			CharacterJump _collidingCharacterJump = corgiController.gameObject.MMGetComponentNoAlloc<Character>()?.FindAbility<CharacterJump>();
			if (_collidingCharacterJump != null)
			{
				_collidingCharacterJump.ResetJumpButtonReleased();
				if (ResetNumberOfJumpsOnStomp)
				{
					_collidingCharacterJump.ResetNumberOfJumps();
				}
			}
		}

		// disable the snail's damage when it's get hurt for some seconds
		IEnumerator ActiveAfter(float seconds)
        {
			yield return new WaitForSeconds(seconds);
			if (_health != null)
			{
				GetComponent<CorgiController>().enabled = true;
				GetComponent<DamageOnTouch>().enabled = true;
				GetComponent<AIWalk>().enabled = true;
				GetComponent<CharacterHorizontalMovement>().enabled = true;
			}
		}
		
	}
}