﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;
using System.Collections.Generic;
using MoreMountains.InventoryEngine;

namespace MoreMountains.CorgiEngine
{
    /// <summary>
    /// Add this component to an empty object in your scene, and when you'll press the SwitchCharacter button (P by default, change that in Unity's InputManager settings), 
    /// your main character will be replaced by one of the prefabs in the list set on this component. You can decide the order (sequential or random), and have as many as you want.
    /// Note that this will change the whole prefab, not just the visuals. 
    /// If you're just after a visual change, look at the CharacterSwitchModel ability.
    /// If you want to swap characters between a bunch of characters within a scene, look at the CharacterSwap ability and CharacterSwapManager
    /// </summary>
    public class MarioCharacterSwitchManager : CharacterSwitchManager
    {
        
    }
}
