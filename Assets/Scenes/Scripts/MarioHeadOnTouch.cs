﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;
using System.Collections.Generic;
using MoreMountains.Feedbacks;
using UnityEngine.Serialization;


namespace MoreMountains.CorgiEngine
{
    public class MarioHeadOnTouch : DamageOnTouch
    {
         protected override void OnCollideWithDamageable(Health health)
         {
            // if what we're colliding with is a CorgiController, we apply a knockback force
            _colliderCorgiController = health.gameObject.MMGetComponentNoAlloc<CorgiController>();

            ApplyDamageCausedKnockback();

            OnHitDamageable?.Invoke();

            HitDamageableFeedback?.PlayFeedbacks(this.transform.position);

            if ((FreezeFramesOnHitDuration > 0) && (Time.timeScale > 0))
            {
                MMFreezeFrameEvent.Trigger(Mathf.Abs(FreezeFramesOnHitDuration));
            }

            _colliderHealth.Damage(DamageCaused, gameObject, InvincibilityDuration, InvincibilityDuration);

            if (_colliderHealth.CurrentHealth <= 0)
            {
                OnKill?.Invoke();
            }

            SelfDamage(DamageTakenEveryTime + DamageTakenDamageable);
         }
    }
}
